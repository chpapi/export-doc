package com.chp.export.util;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.util.Map;

/**
 * @Desc TODO
 * @ClassName WordUtil
 * Author chp
 * Date 2021/10/15 上午11:39
 **/
public class WordUtil {

    //模板路径
    private static final String FTL_FP = "/templates/";
    private static Configuration configuration = null;

    static {
        configuration = new Configuration();
        //设置默认的编码
        configuration.setDefaultEncoding("UTF-8");
    }

    public static Boolean writeWordReport(String wordFilePath, String wordFileName, String templateFileName, Map<String, Object> beanParams) {
        Writer out = null;
        try {
            configuration.setClassForTemplateLoading(WordUtil.class, FTL_FP);
            Template template = configuration.getTemplate(templateFileName, "UTF-8");
            //判断文件后缀
            if (wordFilePath.lastIndexOf(File.separator) != wordFilePath.length() - 1) {
                wordFilePath = wordFilePath + File.separator;
            }
            //获取文件目录，如果不存在则创建
            File file = new File(wordFilePath);
            if (!file.exists()) {
                file.mkdirs();
            }
            //输出文件
            FileOutputStream fos = new FileOutputStream(new File(wordFilePath + wordFileName));
            out = new OutputStreamWriter(fos, "UTF-8");
            template.process(beanParams, out);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
