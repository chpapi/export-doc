package com.chp.export.controller;

import com.chp.export.pojo.ExportRequest;
import com.chp.export.util.WordUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * @Desc TODO
 * @ClassName UserController
 * Author chp
 * Date 2021/9/8 上午11:12
 **/
@Controller
@Api(value = "可视化分析word文件生成下载", tags = {"可视化分析word文件生成下载"})
public class ExportController {

    //文件存储路径
    private final String BASE_PATH = "/Users/chp/Desktop";
    //模板文件名称
    private final String TEMPLATE_FILE_NAME = "export.ftl";

    @GetMapping("/view")
    @ApiOperation("页面跳转")
    public String view() {
        return "view";
    }

    @PostMapping("/exportWord")
    @ResponseBody
    @ApiOperation("word文档生成")
    public String exportWord(@RequestBody @Valid ExportRequest exportRequest, HttpServletRequest req, HttpServletResponse resp) throws IOException {

        //文件生成名称（因为是2003版本的xml模板，这里使用.doc后缀，如果使用.docx后缀生成的文件有问题）
        String wordFileName = LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")) + ".doc";
        //添加模板数据
        Map<String, Object> dataMap = new HashMap<>();
        //添加主题替换内容
        dataMap.put("banner", exportRequest.getBanner());
        //添加标题替换内容
        dataMap.put("title", exportRequest.getTitle());
        //添加描述替换内容
        dataMap.put("desc", exportRequest.getDesc());
        //替换转移字符
        String newImageInfo = exportRequest.getEchartsBase64Str().replaceAll(" ", "+");
        //取出图片部分
        String[] arr = newImageInfo.split("base64,");
        //添加echarts图片替换数据
        dataMap.put("echarts_data", arr[1]);
        //添加table表替换数据
        dataMap.put("table_list", exportRequest.getTableDatas());
        //生成word文档
        Boolean result = WordUtil.writeWordReport(BASE_PATH, wordFileName, TEMPLATE_FILE_NAME, dataMap);
        return BASE_PATH + "/" + wordFileName;

    }

    @ApiOperation("word文档生下载")
    @RequestMapping(value = "/downFile.do", method = RequestMethod.GET)
    public void downPicture(HttpServletRequest request, HttpServletResponse response) throws Exception {
        FileInputStream in = null;
        String filename = request.getParameter("filename");
        String filePath = request.getParameter("filePath");
        filename = new String(filename.getBytes("UTF-8"), "ISO-8859-1");
        try {
            byte[] bs = new byte[1024];
            int len = 0;
            response.reset();
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename=" + filename);
            in = new FileInputStream(new File(filePath));
            ServletOutputStream out = response.getOutputStream();
            while ((len = in.read(bs)) != -1) {
                out.write(bs, 0, len);
            }
            out.flush();
            //关流
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("下载失败");
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
        }

    }
}
