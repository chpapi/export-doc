package com.chp.export.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @Desc TODO
 * @ClassName ExportRequest
 * Author chp
 * Date 2021/10/21 上午10:04
 **/
@Data
@ApiModel(value="可视化分析word文档生成请求参数",description="可视化分析word文档生成请求参数")
public class ExportRequest implements Serializable {

    private static final long serialVersionUID = -1242493306307174690L;

    @NotBlank(message = "主题不能为空!")
    @ApiModelProperty("主题")
    private String banner;

    @NotBlank(message = "标题不能为空!")
    @ApiModelProperty("标题")
    private String title;

    @NotBlank(message = "描述不能为空!")
    @ApiModelProperty("描述")
    private String desc;

    @ApiModelProperty("echarts图转换成的base64字符串")
    private String echartsBase64Str;

    @ApiModelProperty("表格对应每行列数据")
    private List<List<String>> tableDatas;
}
